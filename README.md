# Marpe
An electronic health records and hospital information system. 

## Project Requirements
For development, you need Node.js and a node package manage - Yarn or npm, installed on your environment.

### Node
- #### Installation on Windows
    Download node installer from the official [Node.js website](https://nodejs.org/en/download/), then run it.

    Also, be sure to have `git` available in your PATH, `npm` might need it. You can find git [here](https://git-scm.com/).

- #### Node installation on Ubuntu
    You can install nodejs and npm easily with apt install, just run the following commands.

    ```
    $ sudo apt install nodejs
    $ sudo apt install npm
    ```

- #### Other Operating Systems
  You can find more information about the installation on the [official Node.js website](https://nodejs.org/) and the [official NPM website](https://npmjs.org/).

If the installation was successful, you should be able to run the following commands and receive an output with the respective versions

```
$ node --version
v8.11.3
```

```
$ npm --version
6.1.0
```

If you need to update `npm`
```
$ npm install npm -g
```

## Install Project on local 
```
$ git clone https://gitlab.com/health-information-systems/marpe-node-js.git
$ cd p2p-nodejs-video-chat
$ npm install
```

### Run Project on Windows CMD
```
$ SET DEBUG=marpe-node-js:* & npm start
```

### Run project on Linux/macOS
```
$ DEBUG=marpe-node-js:* npm start
```

